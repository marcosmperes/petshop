import Pets from './components/Pets.vue';
import AdicionarPet from './components/AdicionarPet.vue';
import EditarPet from './components/EditarPet.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Pets
    },
    {
        name: 'add',
        path: '/add',
        component: AdicionarPet
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditarPet
    }
];