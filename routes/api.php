<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('pets', 'PetController@index');
Route::group(['prefix' => 'pet'], function () {
    Route::post('add', 'PetController@add');
    Route::get('edit/{id}', 'PetController@edit');
    Route::post('update/{id}', 'PetController@update');
    Route::delete('delete/{id}', 'PetController@delete');
});
