<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pet;

class PetController extends Controller
{
    public function index()
    {
        $pets = Pet::all()->toArray();
        return array_reverse($pets);
    }

    public function add(Request $request)
    {
        $pet = new Pet([
            'nome' => $request->nome,
            'dono' => $request->dono,
            'raça' => $request->raça,
            'idade' => $request->idade
        ]);
        $pet->save();

        return response()->json('Pet Adicionado.');
    }

    public function edit($id)
    {
        $pet = Pet::find($id);
        return response()->json($pet);
    }

    public function update($id, Request $request)
    {
        $pet = Pet::find($id);
        $pet->update($request->all());

        return response()->json('Pet atualizado.');
    }

    public function delete($id)
    {
        $pet = Pet::find($id);
        $pet->delete();

        return response()->json('Pet excluido.');
    }
}