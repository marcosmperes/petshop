<?php

namespace Tests\Unit;

use Tests\TestCase;

class PetTest extends TestCase
{
    
    public function testIndex()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testPetCreate(){

        $response = $this->post('api/pet/add', [
            'nome' => 'Tica',
            'dono' => 'Pedro',
            'raça' => 'Mastin',
            'idade' => 10            
        ]);

        $this->assertEquals(200, $response->status());

    }

    public function testPetGet(){

        $response = $this->get('api/pet/edit/3');

        $this->assertEquals(200, $response->status());

    }

    public function testPetUpdate(){

        $response = $this->post('api/pet/update/4', [
            'nome' => 'Teste',
            'dono' => 'Teste',
            'raça' => 'Teste',
            'idade' => 10            
        ]);

        $this->assertEquals(200, $response->status());

    }

    public function testPetDelete(){

        $response = $this->delete('api/pet/delete/1');

        $this->assertEquals(200, $response->status());

    }

}
